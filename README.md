### What is this repository for? ###

This python script converts hand-typed addresses to what will hopefully be matched addresses to the official Arlington County Street Codes.

### How do I get set up? ###

* Install [Python 3.8 64-bit](https://www.python.org/downloads/release/python-388/); scroll to the very bottom for the Windows installer
    * You may have another version of Python installed already. That may cause an issue.
* Once wizard has installed, open a command line, and type `python --version`
    * You should get back `Python 3.8.8`
* This python script requires two other modules on top of the python library. On the command prompt:
    *  `pip install pandas`
    *  `pip install openpyxl`
* Open the file in your favorite editor or Notepad, and update the file locations
* Now, run the file: `py acg_addresses.py`

### Relase Notes ###

* I haven't tested the file in any other python version
* The "Street Codes" file must be a XLSX file -- which produces a user warning that can be ignored. Be sure to save the "Street Codes" file as an XLSX and update the file location in the python script

### Who do I talk to? ###

Joseph Crockett

jcrockett@arlingtonva.us