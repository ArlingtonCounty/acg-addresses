#python 3.8.8 64-bit

import pandas
import datetime
import re

patternStreetNumber = '[0-9]+'
patternNumberedStreet = '[0-9]+st|[0-9]+nd|[0-9]+ nd|[0-9]+rd|[0-9]+ rd|[0-9]+th|[0-9]+ th'
patternStreetName = '[A-Z,a-z,0-9," ","."]+'

fileLocationList = [
    {
        'sourceName': 'Preregistration',
        'sourceLocation': 'C:/preregistrants/addresses/2021-05-04_api_viis_records.xlsx',
        'sourceSheetName': 'Sheet1',
        'resultsLocation': 'C:/preregistrants/addresses/2021-05-04_results.xlsx',
        'resultsSheetName': 'Sheet1'
    }
]

addressCodes = {
    'fileName': 'C:/preregistrants/addresses/ARL_Street_Codes.xlsx',
    'sheetName': 'ARL_Street_Codes'
}

addressCodeList = []
records = []
address1_line1_arr = []
troubleshooting_arr = []
isNumberedStreet_arr = []
regex_arr = []

def letsGetTheAddressCodes():
    worksheet = pandas.read_excel(
        addressCodes['fileName'], sheet_name=addressCodes['sheetName'])
    for index, row in worksheet.iterrows():
        addressCodeList.append({
            'STNAME': str(row['STNAME']),
            'STRTNAMELG': str(row['STRTNAMELG'])
            })
    print('finished reading address codes', datetime.datetime.now())

def createRecord(row, source):
    regex = ''
    address1_line1 = ''
    address1_number = ''
    address1_streetName = ''
    isNumberedStreet = False
    numberedStreetDirection = ''
    troubleshooting = ''
    if 'VIIS_Address1' in row:
        if str(row['VIIS_Address1']) != 'nan':
            stNumGrp = re.search(
                patternStreetNumber, row['VIIS_Address1'], flags=0)
            if stNumGrp is not None:
                address1_number = stNumGrp.group(0)
            stNameRaw = row['VIIS_Address1'].replace(address1_number, "")
            stNameGrp = re.search(patternStreetName, stNameRaw, flags=0)
            if stNameGrp is not None:
                address1_streetName = stNameGrp.group(0)
                address1_streetName = address1_streetName.strip()
                #print(address1_streetName)
                troubleshooting = address1_streetName
                for street in addressCodeList:
                    #print(street)
                    if street['STNAME'].lower() == address1_streetName:
                        regex = regex + 'MATCH TO STNAME;'
                        #print(address1_streetName)
                        #print(street['STNAME'])
                        address1_line1 = address1_number + " " + street['STNAME']
                        break
                    
                if regex != 'MATCH TO STNAME;':
                    spaceInNumber = re.search(r'[0-9]+ nd|[0-9]+ rd|[0-9]+ th', address1_streetName, flags=0)
                    if spaceInNumber is not None:
                        troubleshooting = "* 0 SPACE IN NUMBER"
                        #print(spaceInNumber.group(0))
                        address1_streetName = address1_streetName.replace(spaceInNumber.group(0), spaceInNumber.group(0).replace(" ", ""))
                    elif 'first' in address1_streetName:
                        address1_streetName = address1_streetName.replace('first', "1st")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    elif 'second' in address1_streetName:
                        address1_streetName = address1_streetName.replace('second', "2nd")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    elif 'third' in address1_streetName:
                        address1_streetName = address1_streetName.replace('third', "3rd")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    elif 'fourth' in address1_streetName:
                        address1_streetName = address1_streetName.replace('fourth', "4th")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    elif 'fifth' in address1_streetName:
                        address1_streetName = address1_streetName.replace('fifth', "5th")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    elif 'sixth' in address1_streetName:
                        address1_streetName = address1_streetName.replace('sixth', "6th")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    elif 'seventh' in address1_streetName:
                        address1_streetName = address1_streetName.replace('seventh', "7th")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    elif 'eighth' in address1_streetName:
                        address1_streetName = address1_streetName.replace('eighth', "8th")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    elif 'ninth' in address1_streetName:
                        address1_streetName = address1_streetName.replace('ninth', "9th")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    elif 'tenth' in address1_streetName:
                        address1_streetName = address1_streetName.replace('tenth', "10th")
                        troubleshooting = "* 0 WRITTEN NUMBER"
                    for street in addressCodeList:
                        if street['STRTNAMELG'].lower() in address1_streetName:
                            #STRTDIR
                            regex = regex + 'MATCH TO A STRTNAMELG;'
                            isNumberedStreet = False
                            stDirGrp = re.search(
                                patternNumberedStreet, address1_streetName, flags=0)
                            if stDirGrp is not None:
                                # numbered street
                                isNumberedStreet = True
                                northGrp = re.search(r'\bnorth\b|\b n\b|\bn \b|n\.|n$', address1_streetName, flags=0)
                                if northGrp is not None:
                                    address1_line1 = address1_number + " " + street['STRTNAMELG'] + " "
                                    numberedStreetDirection = 'N'
                                southGrp = re.search(r'\bsouth\b|\b s\b|\bs \b|s\.|s$', address1_streetName, flags=0)
                                if southGrp is not None:
                                    address1_line1 = address1_number + " " + street['STRTNAMELG'] + " "
                                    numberedStreetDirection = 'S'
                            else:
                                # non-numbered street
                                northGrp = re.search(r'\bnorth\b|\b n\b|\bn \b|n\.|n$', address1_streetName, flags=0)
                                if northGrp is not None:
                                    address1_line1 = address1_number + " N " + street['STRTNAMELG'] + " "
                                    break
                                southGrp = re.search(r'\bsouth\b|\bs \b|s\.|s$', address1_streetName, flags=0)
                                if southGrp is not None:
                                    address1_line1 = address1_number + " S " + street['STRTNAMELG'] + " "
                                    break
                                regex = regex + 'ELSE'
                                address1_line1 = address1_number + " " + street['STRTNAMELG'] + " "
                                break
                    
                    if 'colombia' in address1_streetName:
                        address1_line1 = address1_number + " COLUMBIA "
                    if len(address1_line1) > 0:
                        #STRTTYPE
                        #ARC
                        grp = re.search(r'\barc\b', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "ARC"

                        #AVE
                        grp = re.search(r'\bave\b|\b av\b|\bavenue\b|av\.|ave\.', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "AVE"                        

                        #BLVD
                        grp = re.search(r'\bblvd\b|\bboulevard\b', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "BLVD"

                        #BRG
                        grp = re.search(r'\bbrg\b', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "BRG"

                        #CIR
                        grp = re.search(r'\bcir\b|\bcirle\b|cir\.', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "CIR"                       

                        #CT
                        grp = re.search(r'\bct\b|\bcourt\b|ct\.', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "CT"

                        #DR
                        grp = re.search(r'\bdr\b|\bdrive\b|dr\.', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "DR"                        

                        #HWY
                        grp = re.search(r'\bhwy\b|\bhighway\b', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "HWY"

                        #LN
                        grp = re.search(r'\bln\b|\blane\b', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "LN"

                        #PIKE
                        grp = re.search(r'\bpike\b|\bpk\b', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "PIKE"

                        #PKWY
                        grp = re.search(r'\bpkwy\b|\bparkway\b', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "PKWY"

                        #PL
                        grp = re.search(r'\bpl\b|\bplace\b|pl\.', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "PL"

                        #RD
                        grp = re.search(r'\brd\b|\broad\b|rd\.', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "RD"

                        #ST
                        grp = re.search(r'\b st\b|\bstreet\b|st\.', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "ST"

                        #TER
                        grp = re.search(r'\bter\b|\bterrace\b|ter\.', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "TER"                       

                        #TNWY
                        grp = re.search(r'\btnwy\b', address1_streetName, flags=0)
                        if grp is not None:
                            address1_line1 = address1_line1 + "TNWY"
                        
                        # is a numbered street, add the direction:
                        if isNumberedStreet:
                            address1_line1 = address1_line1 + " " + numberedStreetDirection
    #if len(address1_line1) > 0:
        #address1_line1 = row['VIIS_Address1']
        #regex = regex + 'fail'
    record = {
        'address1_line1': address1_line1,
        'VIIS_Address1': row['VIIS_Address1'],
        'troubleshooting': troubleshooting,
        'isNumberedStreet': isNumberedStreet,
        'regex': regex,
    }
    records.append(record)
    address1_line1_arr.append(address1_line1)
    troubleshooting_arr.append(troubleshooting)
    isNumberedStreet_arr.append(isNumberedStreet)
    regex_arr.append(regex)

for i in range(0, len(fileLocationList)):
    print('start', datetime.datetime.now())
    letsGetTheAddressCodes()
    # input:
    worksheet = pandas.read_excel(
        fileLocationList[i]['sourceLocation'], sheet_name=fileLocationList[i]['sourceSheetName'])
    print('finished reading file', datetime.datetime.now())

    for index, row in worksheet.iterrows():
        createRecord(row, fileLocationList[i]['sourceName'])
    
    # Adding the questions and related answers back to the dataframe
    worksheet['address1_line1'] = address1_line1_arr
    worksheet['troubleshooting'] = troubleshooting_arr
    worksheet['isNumberedStreet'] = isNumberedStreet_arr
    worksheet['regex'] = regex_arr

    # output:
    recordsDf = pandas.DataFrame(worksheet)
    recordsDf.to_excel(fileLocationList[i]['resultsLocation'],sheet_name=fileLocationList[i]['resultsSheetName'])
    
    print(len(worksheet))  # want to know the number of records
    records[:] = []
    print('end', datetime.datetime.now())
